export const jobs = [
    {
        "author": "John Doe",
        "title": "Software Engineer",
        "salary_range": {
            "from": 80000,
            "to": 100000
        },
        "slug": "software-engineer",
        "vacancies": 3,
        "location": "San Francisco, CA",
        "job_type": "Full-time",
        "applicants": 10,
        "skills_required": ["React", "Node", "MongoDB", "Express"],
        "experience": 2,
        "deadline": "2023-10-15",
        "company": "Google",
        "description": "We are looking for a Software Engineer to join our growing Engineering team and build out the next generation of our platform. The ideal candidate is a hands-on platform builder with significant experience in developing scalable data platforms. We're looking for someone with experience in business intelligence, analytics, data science and data products. They must have strong, firsthand technical expertise in a variety of configuration management and big data technologies and the proven ability to fashion robust scalable solutions that can manage large data sets. They must be at ease working in an agile environment with little supervision. This person should embody a passion for continuous improvement and innovation."
    },
    {
        "author": "Jane Doe",
        "title": "Marketing Manager",
        "salary_range": {
            "from": 60000,
            "to": 80000
        },
        "slug": "marketing-manager",
        "vacancies": 2,
        "location": "New York, NY",
        "job_type": "Full-time",
        "applicants": 5,
        "skills_required": ["SEO", "Social Media Marketing", "Content Marketing"],
        "experience": 1,
        "deadline": "2023-10-20",
        "company": "Facebook",
        "description": "We are looking for a Marketing Manager to join our growing Marketing team and build out the next generation of our platform. The ideal candidate is a hands-on platform builder with significant experience in developing scalable data platforms. We're looking for someone with experience in business intelligence, analytics, data science and data products. They must have strong, firsthand technical expertise in a variety of configuration management and big data technologies and the proven ability to fashion robust scalable solutions that can manage large data sets. They must be at ease working in an agile environment with little supervision. This person should embody a passion for continuous improvement and innovation."
    },
    {
        "author": "John Doe",
        "title": "Data Analyst",
        "salary_range": {
            "from": 70000,
            "to": 90000
        },
        "slug": "data-analyst",
        "vacancies": 1,
        "location": "Chicago, IL",
        "job_type": "Part-time",
        "applicants": 2,
        "skills_required": ["Data Analysis", "Data Visualization", "Data Mining"],
        "experience": 3,
        "deadline": "2023-10-18",
        "company": "Amazon",
        "description": "We are looking for a Data Analyst to join our growing Data Analytics team and build out the next generation of our platform. The ideal candidate is a hands-on platform builder with significant experience in developing scalable data platforms. We're looking for someone with experience in business intelligence, analytics, data science and data products. They must have strong, firsthand technical expertise in a variety of configuration management and big data technologies and the proven ability to fashion robust scalable solutions that can manage large data sets. They must be at ease working in an agile environment with little supervision. This person should embody a passion for continuous improvement and innovation."
    },
    {
        "author": "Jane Doe",
        "title": "Graphic Designer",
        "salary_range": {
            "from": 50000,
            "to": 70000
        },
        "slug": "graphic-designer",
        "vacancies": 1,
        "location": "Los Angeles, CA",
        "job_type": "Contract",
        "applicants": 3,
        "skills_required": ["Adobe Photoshop", "Adobe Illustrator", "Adobe XD"],
        "experience": 2,
        "deadline": "2023-10-25",
        "company": "Apple",
        "description": "We are looking for a Graphic Designer to join our growing Design team and build out the next generation of our platform. The ideal candidate is a hands-on platform builder with significant experience in developing scalable data platforms. We're looking for someone with experience in business intelligence, analytics, data science and data products. They must have strong, firsthand technical expertise in a variety of configuration management and big data technologies and the proven ability to fashion robust scalable solutions that can manage large data sets. They must be at ease working in an agile environment with little supervision. This person should embody a passion for continuous improvement and innovation."
    },
    {
        "author": "John Doe",
        "title": "Product Manager",
        "salary_range": {
            "from": 90000,
            "to": 120000
        },
        "slug": "product-manager",
        "vacancies": 2,
        "location": "Austin, TX",
        "job_type": "Full-time",
        "applicants": 7,
        "skills_required": ["Product Management", "Product Development", "Product Strategy"],
        "experience": 4,
        "deadline": "2023-10-22",
        "company": "Microsoft",
        "description": "We are looking for a Product Manager to join our growing Product team and build out the next generation of our platform. The ideal candidate is a hands-on platform builder with significant experience in developing scalable data platforms. We're looking for someone with experience in business intelligence, analytics, data science and data products. They must have strong, firsthand technical expertise in a variety of configuration management and big data technologies and the proven ability to fashion robust scalable solutions that can manage large data sets. They must be at ease working in an agile environment with little supervision. This person should embody a passion for continuous improvement and innovation."
    },
    {
        "author": "Jane Doe",
        "title": "Sales Representative",
        "salary_range": {
            "from": 45000,
            "to": 60000
        },
        "slug": "sales-representative",
        "vacancies": 3,
        "location": "Miami, FL",
        "job_type": "Full-time",
        "applicants": 8,
        "skills_required": ["Sales", "Marketing", "Communication"],
        "experience": 1,
        "deadline": "2023-10-17",
        "company": "Netflix",
        "description": "We are looking for a Sales Representative to join our growing Sales team and build out the next generation of our platform. The ideal candidate is a hands-on platform builder with significant experience in developing scalable data platforms. We're looking for someone with experience in business intelligence, analytics, data science and data products. They must have strong, firsthand technical expertise in a variety of configuration management and big data technologies and the proven ability to fashion robust scalable solutions that can manage large data sets. They must be at ease working in an agile environment with little supervision. This person should embody a passion for continuous improvement and innovation."
    },
    {
        "author": "John Doe",
        "title": "HR Specialist",
        "salary_range": {
            "from": 55000,
            "to": 75000
        },
        "slug": "hr-specialist",
        "vacancies": 1,
        "location": "Denver, CO",
        "job_type": "Part-time",
        "applicants": 3,
        "skills_required": ["Human Resources", "Recruitment", "Employee Relations"],
        "experience": 2,
        "deadline": "2023-10-19",
        "company": "Tesla",
        "description": "We are looking for a HR Specialist to join our growing HR team and build out the next generation of our platform. The ideal candidate is a hands-on platform builder with significant experience in developing scalable data platforms. We're looking for someone with experience in business intelligence, analytics, data science and data products. They must have strong, firsthand technical expertise in a variety of configuration management and big data technologies and the proven ability to fashion robust scalable solutions that can manage large data sets. They must be at ease working in an agile environment with little supervision. This person should embody a passion for continuous improvement and innovation."
    },
    {
        "author": "Jane Doe",
        "title": "Financial Analyst",
        "salary_range": {
            "from": 75000,
            "to": 95000
        },
        "slug": "financial-analyst",
        "vacancies": 1,
        "location": "Seattle, WA",
        "job_type": "Full-time",
        "applicants": 4,
        "skills_required": ["Financial Analysis", "Financial Reporting", "Financial Modeling"],
        "experience": 3,
        "deadline": "2023-10-21",
        "company": "Twitter",
        "description": "We are looking for a Financial Analyst to join our growing Finance team and build out the next generation of our platform. The ideal candidate is a hands-on platform builder with significant experience in developing scalable data platforms. We're looking for someone with experience in business intelligence, analytics, data science and data products. They must have strong, firsthand technical expertise in a variety of configuration management and big data technologies and the proven ability to fashion robust scalable solutions that can manage large data sets. They must be at ease working in an agile environment with little supervision. This person should embody a passion for continuous improvement and innovation."
    },
    {
        "author": "John Doe",
        "title": "Content Writer",
        "salary_range": {
            "from": 40000,
            "to": 55000
        },
        "slug": "content-writer",
        "vacancies": 2,
        "location": "Boston, MA",
        "job_type": "Contract",
        "applicants": 6,
        "skills_required": ["Content Writing", "Copywriting", "Creative Writing"],
        "experience": 1,
        "deadline": "2023-10-24",
        "company": "Uber",
        "description": "We are looking for a Content Writer to join our growing Content team and build out the next generation of our platform. The ideal candidate is a hands-on platform builder with significant experience in developing scalable data platforms. We're looking for someone with experience in business intelligence, analytics, data science and data products. They must have strong, firsthand technical expertise in a variety of configuration management and big data technologies and the proven ability to fashion robust scalable solutions that can manage large data sets. They must be at ease working in an agile environment with little supervision. This person should embody a passion for continuous improvement and innovation."
    },
    {
        "author": "Jane Doe",
        "title": "Customer Support Specialist",
        "salary_range": {
            "from": 50000,
            "to": 65000
        },
        "slug": "customer-support-specialist",
        "vacancies": 3,
        "location": "Philadelphia, PA",
        "job_type": "Full-time",
        "applicants": 9,
        "skills_required": ["Customer Service", "Customer Support", "Communication"],
        "experience": 1,
        "deadline": "2023-10-16",
        "company": "Airbnb",
        "description": "We are looking for a Customer Support Specialist to join our growing Customer Support team and build out the next generation of our platform. The ideal candidate is a hands-on platform builder with significant experience in developing scalable data platforms. We're looking for someone with experience in business intelligence, analytics, data science and data products. They must have strong, firsthand technical expertise in a variety of configuration management and big data technologies and the proven ability to fashion robust scalable solutions that can manage large data sets. They must be at ease working in an agile environment with little supervision. This person should embody a passion for continuous improvement and innovation."
    },
    {
        "author": "John Doe",
        "title": "UX Designer",
        "salary_range": {
            "from": 70000,
            "to": 90000
        },
        "slug": "ux-designer",
        "vacancies": 1,
        "location": "San Diego, CA",
        "job_type": "Full-time",
        "applicants": 3,
        "skills_required": ["User Experience", "User Interface", "User Research"],
        "experience": 2,
        "deadline": "2023-10-23",
        "company": "PayPal",
        "description": "We are looking for a UX Designer to join our growing Design team and build out the next generation of our platform. The ideal candidate is a hands-on platform builder with significant experience in developing scalable data platforms. We're looking for someone with experience in business intelligence, analytics, data science, and data products. They must have strong, firsthand technical expertise in a variety of configuration management and big data technologies and the proven ability to fashion robust scalable solutions that can manage large datasets. They must be at ease working in an agile environment with little supervision. This person should embody a passion for continuous improvement and innovation."
    },
    {
        "author": "Jane Doe",
        "title": "Project Manager",
        "salary_range": {
            "from": 85000,
            "to": 110000
        },
        "slug": "project-manager",
        "vacancies": 2,
        "location": "Houston, TX",
        "job_type": "Full-time",
        "applicants": 5,
        "skills_required": ["Project Management", "Project Planning", "Project Coordination"],
        "experience": 3,
        "deadline": "2023-10-18",
        "company": "Shopify",
        "description": "We are looking for a Project Manager to join our growing Project Management team and build out the next generation of our platform. The ideal candidate is a hands-on platform builder with significant experience in developing scalable data platforms. We are looking for someone with experience in business intelligence, analytics, data science, and data products. They must have strong, firsthand technical expertise in a variety of configuration management and big data technologies and the proven ability to fashion robust scalable solutions that can manage large datasets. They must be at ease working in an agile environment with little supervision. This person should embody a passion for continuous improvement and innovation."
    },
    {
        "author": "John Doe",
        "title": "Administrative Assistant",
        "salary_range": {
            "from": 35000,
            "to": 45000
        },
        "slug": "administrative-assistant",
        "vacancies": 1,
        "location": "Atlanta, GA",
        "job_type": "Part-time",
        "applicants": 2,
        "skills_required": ["Administrative Support", "Data Entry", "Microsoft Office"],
        "experience": 1,
        "deadline": "2023-10-20",
        "company": "LinkedIn",
        "description": "We are looking for an Administrative Assistant to join our growing Administrative team and build out the next generation of our platform. The ideal candidate is a hands-on platform builder with significant experience in developing scalable data platforms. We are looking for someone with experience in business intelligence, analytics, data science, and data products. They must have strong, firsthand technical expertise in a variety of configuration management and big data technologies and the proven ability to fashion robust scalable solutions that can manage large datasets. They must be at ease working in an agile environment with little supervision. This person should embody a passion for continuous improvement and innovation."
    },
    {
        "author": "Jane Doe",
        "title": "Quality Assurance Engineer",
        "salary_range": {
            "from": 60000,
            "to": 75000
        },
        "slug": "qa-engineer",
        "vacancies": 1,
        "location": "Portland, OR",
        "job_type": "Full-time",
        "applicants": 4,
        "skills_required": ["Quality Assurance", "Software Testing", "Test Automation"],
        "experience": 2,
        "deadline": "2023-10-19",
        "company": "Adobe",
        "description": "We are looking for a Quality Assurance Engineer to join our growing Quality Assurance team and build out the next generation of our platform. The ideal candidate is a hands-on platform builder with significant experience in developing scalable data platforms. We are looking for someone with experience in business intelligence, analytics, data science, and data products. They must have strong, firsthand technical expertise in a variety of configuration management and big data technologies and the proven ability to fashion robust scalable solutions that can manage large datasets. They must be at ease working in an agile environment with little supervision. This person should embody a passion for continuous improvement and innovation."
    },
    {
        "author": "John Doe",   
        "title": "Social Media Manager",
        "salary_range": {
            "from": 45000,
            "to": 60000
        },
        "slug": "software-engineer",
        "vacancies": 2,
        "location": "Phoenix, AZ",
        "job_type": "Contract",
        "applicants": 6,
        "skills_required": ["Social Media Marketing", "Content Creation", "Social Media Management"],
        "experience": 1,
        "deadline": "2023-10-22",
        "company": "GitHub",
        "description": "We are looking for a Social Media Manager to join our growing Social Media team and build out the next generation of our platform. The ideal candidate is a hands-on platform builder with significant experience in developing scalable data platforms. We are looking for someone with experience in business intelligence, analytics, data science, and data products. They must have strong, firsthand technical expertise in a variety of configuration management and big data technologies and the proven ability to fashion robust scalable solutions that can manage large datasets. They must be at ease working in an agile environment with little supervision. This person should embody a passion for continuous improvement and innovation."
    }
]