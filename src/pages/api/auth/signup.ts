import { NextApiRequest, NextApiResponse } from "next";
import { DbConnection } from "@/pages/utils/DbConnection";
import { User } from "@/models/User";
import { genSalt, hash } from "bcryptjs";
import { sign } from "jsonwebtoken";

const handler = async (req: NextApiRequest, res: NextApiResponse) => {
    await DbConnection();

    if (req.method !== "POST") return res.status(400).json({ error: "Method not allowed." });

    try {
        let user = await User.findOne({ username: req.body.username });
        if (user) return res.status(409).json({ message: "Username already exists." });

        user = await User.findOne({ email: req.body.email })
        if (user) return res.status(409).json({ error: "Email belongs to another account." })

        // generating salt and hashing password
        const salt = await genSalt(10)
        req.body.password = await hash(req.body.password, salt)

        // converting date string into date
        req.body.date_of_birth = new Date(req.body.date_of_birth)

        // creating new user
        user = await User.create(req.body)
        await user.save()

        const payload = {
            user: {
                id: user._id
            }
        }

        const token = sign(payload, process.env.JWT_SECRET!);
        res.status(201).json({ token, success: "Account created successfully." });
    } catch (error) {
        res.status(500).json(error);
    }
}

export default handler;