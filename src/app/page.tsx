"use client"

import Image from "next/image";
import { features } from "@/data/features";
import FeatureCard from "@/components/FeatureCard/FeatureCard";
import Link from "next/link";
import JobListItem from "@/components/JobListItem/JobListItem";
import { jobs } from "@/data/Jobs";
import { resources } from "@/data/resources";
import ResourceCard from "@/components/ResourceCard/ResourceCard";
import { useRef, useState } from "react";

export default function Home() {
    const [searchQuery, setSearchQuery] = useState<string>("");
    const redirectLinkRef = useRef<HTMLAnchorElement | null>(null);

    const searchHandler = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        if (!searchQuery) return;
        if (redirectLinkRef.current)
            redirectLinkRef.current.click();

        setSearchQuery("");
    }

    return (
        <main>
            <section className="px-12 min-h-[600px] max-h-600 flex justify-between gap-4 overflow-hidden bg-gradient-to-r from-sky-500 to-indigo-500">
                {/* hero left */}
                <div className="flex-[2] flex flex-col justify-center">
                    <h1 className="text-[8rem] font-bold leading-none text-white">JobFolio</h1>
                    <h3 className="text-[2rem] font-[600] leading-tight my-4 text-white">Linking Talent. Unlocking Potential.</h3>
                    <p className="text-gray-200 mb-20">The ultimate job board connecting top talent with amazing opportunities. Our platform leverages cutting-edge technology and vast industry connections to match job seekers with their dream career and help companies find the perfect fit. Unlock your full potential today with JobFolio.</p>
                    {/* search form */}
                    <form className="flex p-2 items-center border-blue-400 border-2 rounded-xl overflow-hidden bg-white" onSubmit={searchHandler}>
                        <input type="search" value={searchQuery} onChange={(e) => setSearchQuery(e.target.value)} placeholder="Search for jobs, talents, and more.." className="bg-transparent w-full outline-none text-lg pl-2" />
                        <button type="submit" className="bg-blue-500 text-white py-2 px-8 text-lg rounded">Search</button>
                        <Link href={{ pathname: "/jobs", query: { query: searchQuery } }} ref={redirectLinkRef} hidden></Link>
                    </form>
                </div>
                {/* hero right */}
                <div className="flex-[1] flex items-start justify-center h-full min-w-[400px]">
                    <Image src={'/hero.png'} alt={"Guy standing confidentally"} height={10000} width={10000} draggable="false" className="w-[100%] h-full object-cover" />
                </div>
            </section>

            <section className="min-h-[300px] py-12 px-12">
                <h2 className="text-blue-500 text-[3rem] font-[600] text-center mb-10">Why JobFolio?</h2>

                {/* features */}
                <div className="flex justify-center flex-wrap gap-6">
                {/* feature */}
                    {
                        features.map((feature, index) => {
                        return <FeatureCard key={index} feature={feature} />
                        })
                    }
                </div>
            </section>

            <section className="py-8 px-12 flex flex-col items-center bg-gradient-to-t from-sky-100 to-indigo-500">
                <h2 className="text-white text-[3rem] font-[600] text-center mb-10">Recommended Jobs!</h2>
                {/* jobs */}
                <div className="flex flex-col items-center w-full gap-8">
                {/* job */}
                    {
                        jobs.slice(0, 10).map((job, index) => <JobListItem key={index} job={{ title: job.title as string, salary_range: job.salary_range, location: job.location, job_type: job.job_type, deadline: job.deadline, slug: job.slug }} />)
                    }
                </div>
                <Link href={"/jobs"} className="bg-blue-500 text-white py-4 px-12 text-lg rounded mt-10">See More</Link>
            </section>

            <section className="py-12 px-12">
                <h2 className="text-blue-500 text-[3rem] font-[600] text-center mb-10">Recommended Resources</h2>
                <div className="flex flex-wrap justify-center gap-8">
                    {
                        resources.slice(0, 6).map((resource, index) => <ResourceCard key={index} resource={resource} />)
                    }
                    <Link href={"/resources"} className="bg-blue-500 text-white py-4 px-12 text-lg rounded mt-10">
                        See More
                    </Link>
                </div>
            </section>
        </main>
    )
}