"use client"

import './globals.css'
import Navbar from '@/components/Navbar/Navbar'
import Footer from '@/components/Footer/Footer'
import Mailing from '@/components/MailingForm/Mailing'
import { usePathname } from 'next/navigation'

export default function RootLayout({
  children,
}: {
  children: React.ReactNode
}) {
    const pathname = usePathname();
    console.log(pathname != ("/auth/signup" || "/auth/login"))

    return (
        <html lang="en">
            <head />
            <body>
                <Navbar />
                {children}
                {
                    pathname != "/auth/signup" && "auth/login" ?
                    <>
                        <Mailing />
                        <Footer />
                    </>
                    :
                    null
                }
            </body>
        </html>
    )
}
